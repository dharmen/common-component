package com.azilen.test;

import java.util.Collection;

import org.hibernate.SessionFactory;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import com.azilen.common.persistance.IEntity;
import com.azilen.common.persistance.util.UserHolder;
import com.azilen.persistance.IDummyEntityManager;

@ContextConfiguration({"/testApplicationContext.xml"})
public abstract class BaseManagerTest extends AbstractTransactionalJUnit4SpringContextTests {

  @Autowired
  protected SessionFactory testSessionFactory;

  @Autowired
  protected IDummyEntityManager dummyEntityMgr;

  protected static final String TEST_USERID = "Junit";

  @Before
  public void onSetUp() throws Exception {
    UserHolder.setCurrentUser(TEST_USERID);
  }

  protected void flushSession() {
    testSessionFactory.getCurrentSession().flush();
  }

  protected void flushAndClearSession() {
    flushSession();
    testSessionFactory.getCurrentSession().clear();
  }

  protected void evictObject(IEntity entity) {
    testSessionFactory.getCurrentSession().evict(entity);
  }

  protected void evictAll(Collection<IEntity> entities) {
    for (IEntity anEntity : entities)
      testSessionFactory.getCurrentSession().evict(anEntity);
  }
}
