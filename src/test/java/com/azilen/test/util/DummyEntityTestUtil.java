package com.azilen.test.util;

import java.util.Random;

import com.azilen.persistance.impl.DummyEntity;

public class DummyEntityTestUtil {

  public static DummyEntity getNewDummyEntity() {
    DummyEntity entity = new DummyEntity();

    entity.setDummyString("" + System.nanoTime());
    entity.setDummyInt((int) new Random().nextInt(5000));

    return entity;
  }
}
