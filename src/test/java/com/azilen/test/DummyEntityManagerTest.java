package com.azilen.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.Test;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import com.azilen.persistance.impl.DummyEntity;
import com.azilen.test.util.DummyEntityTestUtil;

@Rollback(false)
@Transactional(transactionManager="testTxManager")
public class DummyEntityManagerTest extends BaseManagerTest {

  @Test
  public void testGetByIdDummyEntity() {

    DummyEntity entity = DummyEntityTestUtil.getNewDummyEntity();
    dummyEntityMgr.save(entity);

    flushAndClearSession();

    assertNotNull("DummyEntity object has not been saved correctly.", entity.getId());
    DummyEntity savedEntity = dummyEntityMgr.getById(entity.getId());

    assertEquals(entity.getId(), savedEntity.getId());
    assertEquals(entity.getDummyString(), savedEntity.getDummyString());
    assertEquals(entity.getDummyInt(), savedEntity.getDummyInt());
  }

  @Test
  public void testUpdateDummyEntity() {

    DummyEntity entity = DummyEntityTestUtil.getNewDummyEntity();
    dummyEntityMgr.save(entity);

    assertNotNull("DummyEntity object has not been saved correctly.", entity.getId());
    flushAndClearSession();

    DummyEntity savedEntity = dummyEntityMgr.getById(entity.getId());
    assertEquals(entity.getId(), savedEntity.getId());
    assertEquals(entity.getDummyString(), savedEntity.getDummyString());
    assertEquals(entity.getDummyInt(), savedEntity.getDummyInt());

    savedEntity.setDummyInt(5555);
    savedEntity.setDummyString("5555");
    dummyEntityMgr.save(savedEntity);

    DummyEntity updatedEntity = dummyEntityMgr.getById(savedEntity.getId());
    assertEquals(savedEntity.getId(), updatedEntity.getId());
    assertEquals(savedEntity.getDummyString(), updatedEntity.getDummyString());
    assertEquals(savedEntity.getDummyInt(), updatedEntity.getDummyInt());
  }

  @Test
  public void testDeleteDummyEntity() {
    DummyEntity entity = DummyEntityTestUtil.getNewDummyEntity();
    dummyEntityMgr.save(entity);
    assertNotNull("DummyEntity object has not been saved correctly.", entity.getId());
    flushAndClearSession();

    dummyEntityMgr.delete(entity);

    DummyEntity savedEntity = dummyEntityMgr.getById(entity.getId());
    assertNull("DummyEntity object was not deleted", savedEntity);
  }

  @Test
  public void testDeleteAll() {
    List<DummyEntity> entities = dummyEntityMgr.getAll();
    dummyEntityMgr.deleteAll(entities);
    flushAndClearSession();

    List<DummyEntity> remainingEntities = dummyEntityMgr.getAll();
    assertEquals("All Entities are not deleted", remainingEntities.size(), 0);
  }

}
