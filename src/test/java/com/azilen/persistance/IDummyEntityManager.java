package com.azilen.persistance;

import com.azilen.common.persistance.IEntityManager;
import com.azilen.persistance.impl.DummyEntity;

public interface IDummyEntityManager extends IEntityManager<DummyEntity> {

}
