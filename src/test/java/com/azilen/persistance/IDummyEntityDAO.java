package com.azilen.persistance;

import com.azilen.common.persistance.IEntityDAO;
import com.azilen.persistance.impl.DummyEntity;

public interface IDummyEntityDAO extends IEntityDAO<DummyEntity> {

}
