package com.azilen.persistance.impl;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.envers.Audited;

import com.azilen.common.persistance.AbstractEntity;
import com.azilen.common.persistance.event.annotation.EntityListenerClasses;
import com.azilen.persistance.event.listener.SpringListener;

@Entity
@Table(name = "DUMMY_ENTITY")
@Audited
// @SQLDelete(sql = "update DUMMY_ENTITY set deleted=1 where id=?")
@EntityListenerClasses({SpringListener.class})
public class DummyEntity extends AbstractEntity {

  private String dummyString;
  private int dummyInt;

  @Column(name = "DUMMY_STRING")
  public String getDummyString() {
    return dummyString;
  }

  public void setDummyString(String dummyString) {
    this.dummyString = dummyString;
  }

  @Column(name = "DUMMY_INT")
  public int getDummyInt() {
    return dummyInt;
  }

  public void setDummyInt(int dummyInt) {
    this.dummyInt = dummyInt;
  }

  @Transient
  public String getDescription() {
    return getDummyString() + " " + getDummyInt();
  }

}
