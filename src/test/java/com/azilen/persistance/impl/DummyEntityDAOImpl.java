package com.azilen.persistance.impl;

import org.springframework.stereotype.Component;

import com.azilen.common.persistance.AbstractEntityDAO;
import com.azilen.persistance.IDummyEntityDAO;

@Component
public class DummyEntityDAOImpl extends AbstractEntityDAO<DummyEntity> implements IDummyEntityDAO {

  @Override
  public Class<DummyEntity> getTargetClass() {
    return DummyEntity.class;
  }

}
