package com.azilen.persistance.impl;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.azilen.common.persistance.AbstractEntityManager;
import com.azilen.persistance.IDummyEntityDAO;
import com.azilen.persistance.IDummyEntityManager;

@Component
//@Transactional(readOnly = true)
public class DummyEntityManagerImpl extends AbstractEntityManager<DummyEntity, IDummyEntityDAO> implements
    IDummyEntityManager {

}
