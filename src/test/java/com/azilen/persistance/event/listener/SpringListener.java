package com.azilen.persistance.event.listener;

import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

import org.springframework.stereotype.Component;

import com.azilen.common.persistance.event.listener.EntityListener;
import com.azilen.persistance.impl.DummyEntity;

/**
 * Simple Spring EntityListener.
 * 
 */
@Component
public class SpringListener implements EntityListener {

  @PrePersist
  public void prePersist(DummyEntity entity) {
    System.out.println("Spring PrePersist, key: " + entity.getDummyString());
  }

  @PostPersist
  public void postPersist(DummyEntity entity) {
    System.out.println("Spring PostPersist, key: " + entity.getDummyString());
  }

  @PreUpdate
  public void preUpdate(DummyEntity entity) {
    System.out.println("Spring PreUpdate, key: " + entity.getDummyString());
  }

  @PostUpdate
  public void postUpdate(DummyEntity entity) {
    System.out.println("Spring PostUpdate, key: " + entity.getDummyString());
  }

  @PreRemove
  public void preRemove(DummyEntity entity) {
    System.out.println("Spring PreRemove, key: " + entity.getDummyString());
  }

  @PostRemove
  public void postRemove(DummyEntity entity) {
    System.out.println("Spring PostRemove, key: " + entity.getDummyString());
  }

  @PostLoad
  public void postLoad(DummyEntity entity) {
    System.out.println("Spring PostLoad, key: " + entity.getDummyString());
  }
}
