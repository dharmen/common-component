package com.azilen.common.persistance.util;

public class UserHolder {

  private static ThreadLocal<String> currentUser = new ThreadLocal<String>();

  public static void setCurrentUser(String userId) {
    currentUser.set(userId);
  }

  public static String getCurrentUser() {
    String returnString = (String) currentUser.get();
    return returnString;
  }

}
