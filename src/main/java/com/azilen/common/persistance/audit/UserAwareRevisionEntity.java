package com.azilen.common.persistance.audit;

import org.hibernate.envers.RevisionEntity;
import org.hibernate.envers.DefaultRevisionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "AUDIT_REV_INFO")
@RevisionEntity(UserAwareRevisionListener.class)
public class UserAwareRevisionEntity extends DefaultRevisionEntity {

  private static final long serialVersionUID = 1L;

  @Column(name = "USER_ID")
  private String userId;

  public String getUserId() {
    return this.userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }
}
