package com.azilen.common.persistance.audit;

import org.hibernate.envers.RevisionListener;

import com.azilen.common.persistance.util.UserHolder;

public class UserAwareRevisionListener implements RevisionListener {

  @Override
  public void newRevision(Object revisionEntity) {
    UserAwareRevisionEntity revEntity = (UserAwareRevisionEntity) revisionEntity;
    revEntity.setUserId(UserHolder.getCurrentUser());
  }

}
