package com.azilen.common.persistance;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.azilen.common.persistance.id.ObjectId;

@MappedSuperclass
public abstract class AbstractEntity implements IEntity {

  private ObjectId id;

  @Id
  @Type(type = "com.azilen.common.persistance.id.ObjectIdUserType")
  @GeneratedValue(generator = "oid")
  @GenericGenerator(name = "oid", strategy = "com.azilen.common.persistance.id.ObjectIdGenerator")
  @Column(name = "ID", updatable = false, nullable = false)
  public ObjectId getId() {
    return id;
  }

  public void setId(ObjectId id) {
    this.id = id;
  }

  @Override
  public boolean equals(Object object) {
    if (object == this) {
      return true;
    }

    if (object instanceof AbstractEntity) {
      AbstractEntity otherInstance = (AbstractEntity) object;
      if (otherInstance.getId() == null || this.getId() == null) {
        return false;
      }

      return otherInstance.getId().equals(this.getId());
    } else {
      return false;
    }
  }

  @Override
  public int hashCode() {
    if (this.getId() == null) {
      return 0;
    }
    return this.getId().hashCode();
  }

}
