package com.azilen.common.persistance;

import java.util.Collection;
import java.util.List;

import com.azilen.common.persistance.id.ObjectId;

public interface IEntityDAO<ET extends IEntity> {

  public void save(ET record);

  public void delete(ET entity);

  public void deleteAll(Collection<ET> entities);

  public ET getById(ObjectId id);

  public List<ET> getAll();

}
