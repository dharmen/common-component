package com.azilen.common.persistance;

import java.util.Collection;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import com.azilen.common.persistance.id.ObjectId;

public abstract class AbstractEntityDAO<ET extends IEntity> extends HibernateDaoSupport implements IEntityDAO<ET> {

  public abstract Class<ET> getTargetClass();

  @Autowired
  private void init(SessionFactory sessionFactory) {
    setSessionFactory(sessionFactory);
  }

  @Override
  public void save(ET record) {

    getHibernateTemplate().saveOrUpdate(record);

  }

  @Override
  public void delete(ET entity) {

    getHibernateTemplate().delete(entity);

  }

  @Override
  public void deleteAll(Collection<ET> entities) {

    getHibernateTemplate().deleteAll(entities);

  }

  @Override
  public ET getById(ObjectId id) {

    ET returnIEntityType = (ET) getHibernateTemplate().get(getTargetClass(), id);

    return returnIEntityType;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ET> getAll() {

    Criteria criteria = currentSession().createCriteria(getTargetClass());
    criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
    List<ET> returnList = criteria.list();

    return returnList;
  }

}
