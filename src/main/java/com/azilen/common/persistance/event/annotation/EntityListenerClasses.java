package com.azilen.common.persistance.event.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.azilen.common.persistance.event.listener.EntityListener;

/**
 * Used to list the Hibernate Entity Listener classes
 * 
 */

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface EntityListenerClasses {
  Class<? extends EntityListener>[] value();
}
