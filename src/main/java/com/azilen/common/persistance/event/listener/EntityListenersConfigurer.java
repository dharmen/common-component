package com.azilen.common.persistance.event.listener;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.internal.SessionFactoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.azilen.common.persistance.event.annotation.EntityListenerClasses;

/**
 * This component will setup the wiring of entities and their listeners
 * 
 */

@Component
public class EntityListenersConfigurer {

  private static final Logger log = Logger.getLogger(EntityListenersConfigurer.class);

  @Autowired
  ApplicationContext applicationContext;

  @Autowired
  private SessionFactory sf;

  @Resource
  private List<String> entityClasses;

  @PostConstruct
  private void registerListeners() {

    final Set<EntityListener> listeners = new HashSet<EntityListener>();

    for (String entityClass : this.entityClasses) {

      Class<?> clazz = null;
      try {
        clazz = Class.forName(entityClass);
      } catch (ClassNotFoundException e) {
        log.error("Could not load class [" + entityClass + "]. No entity listener will be wired for this class.", e);
      }

      if (clazz != null && clazz.isAnnotationPresent(EntityListenerClasses.class)) {
        EntityListenerClasses annotation = (EntityListenerClasses) clazz.getAnnotation(EntityListenerClasses.class);

        if (annotation.value() != null) {
          for (Class<? extends EntityListener> listenerBeanClass : annotation.value()) {
            Map<String, ? extends EntityListener> map = this.applicationContext.getBeansOfType(listenerBeanClass);
            listeners.addAll(map.values());
          }
        }
      }
    }

    if (!listeners.isEmpty()) {

      EventListenerRegistry registry =
          ((SessionFactoryImpl) this.sf).getServiceRegistry().getService(EventListenerRegistry.class);

      HibernateEntityListenersAdapter adapter = new HibernateEntityListenersAdapter(listeners);

      registry.getEventListenerGroup(EventType.PRE_INSERT).appendListener(adapter);
      registry.getEventListenerGroup(EventType.POST_INSERT).appendListener(adapter);
      registry.getEventListenerGroup(EventType.PRE_UPDATE).appendListener(adapter);
      registry.getEventListenerGroup(EventType.POST_UPDATE).appendListener(adapter);
      registry.getEventListenerGroup(EventType.PRE_DELETE).appendListener(adapter);
      registry.getEventListenerGroup(EventType.POST_DELETE).appendListener(adapter);
      registry.getEventListenerGroup(EventType.POST_LOAD).appendListener(adapter);

    }
  }

}
