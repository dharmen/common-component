package com.azilen.common.persistance.event.listener;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

import org.apache.log4j.Logger;
import org.hibernate.event.spi.EventType;
import org.hibernate.event.spi.PostDeleteEvent;
import org.hibernate.event.spi.PostDeleteEventListener;
import org.hibernate.event.spi.PostInsertEvent;
import org.hibernate.event.spi.PostInsertEventListener;
import org.hibernate.event.spi.PostLoadEvent;
import org.hibernate.event.spi.PostLoadEventListener;
import org.hibernate.event.spi.PostUpdateEvent;
import org.hibernate.event.spi.PostUpdateEventListener;
import org.hibernate.event.spi.PreDeleteEvent;
import org.hibernate.event.spi.PreDeleteEventListener;
import org.hibernate.event.spi.PreInsertEvent;
import org.hibernate.event.spi.PreInsertEventListener;
import org.hibernate.event.spi.PreUpdateEvent;
import org.hibernate.event.spi.PreUpdateEventListener;
import org.hibernate.persister.entity.EntityPersister;

public class HibernateEntityListenersAdapter implements PostInsertEventListener, PreInsertEventListener,
    PreUpdateEventListener, PostUpdateEventListener, PreDeleteEventListener, PostDeleteEventListener,
    PostLoadEventListener {

  private static final long serialVersionUID = 1L;
  private static final Logger log = Logger.getLogger(HibernateEntityListenersAdapter.class);

  private Set<EntityListener> listeners;

  private Map<Class<?>, Map<Method, EntityListener>> preInsert = new HashMap<Class<?>, Map<Method, EntityListener>>();
  private Map<Class<?>, Map<Method, EntityListener>> postInsert = new HashMap<Class<?>, Map<Method, EntityListener>>();
  private Map<Class<?>, Map<Method, EntityListener>> preUpdate = new HashMap<Class<?>, Map<Method, EntityListener>>();
  private Map<Class<?>, Map<Method, EntityListener>> postUpdate = new HashMap<Class<?>, Map<Method, EntityListener>>();
  private Map<Class<?>, Map<Method, EntityListener>> preRemove = new HashMap<Class<?>, Map<Method, EntityListener>>();
  private Map<Class<?>, Map<Method, EntityListener>> postRemove = new HashMap<Class<?>, Map<Method, EntityListener>>();
  private Map<Class<?>, Map<Method, EntityListener>> postLoad = new HashMap<Class<?>, Map<Method, EntityListener>>();

  public HibernateEntityListenersAdapter(Set<EntityListener> listeners) {
    this.listeners = listeners;
    findMethods();
  }

  private void findMethods() {
    if (log.isDebugEnabled()) {
      log.debug("Registering action events for EventListener classes : " + this.listeners.toArray().toString());
    }

    for (EntityListener listener : this.listeners) {
      findMethodsForListener(listener);
    }
  }

  private void findMethodsForListener(EntityListener listener) {
    Class<? extends EntityListener> listenerClass = listener.getClass();

    for (Method aMethod : listenerClass.getMethods()) {
      if (Void.TYPE.equals(aMethod.getReturnType())) {

        Class<?>[] types = aMethod.getParameterTypes();

        if (types.length == 1) {

          if (aMethod.getAnnotation(PrePersist.class) != null) {
            if (!preInsert.containsKey(types[0])) {
              preInsert.put(types[0], new HashMap<Method, EntityListener>());
            }
            preInsert.get(types[0]).put(aMethod, listener);

            if (log.isDebugEnabled()) {
              log.debug("EventListener Registration : " + EventType.PRE_INSERT.toString() + " - " + types[0] + " - "
                  + listener.getClass().getName());
            }
          }

          if (aMethod.getAnnotation(PostPersist.class) != null) {
            if (!postInsert.containsKey(types[0])) {
              postInsert.put(types[0], new HashMap<Method, EntityListener>());
            }
            postInsert.get(types[0]).put(aMethod, listener);

            if (log.isDebugEnabled()) {
              log.debug("EventListener Registration : " + EventType.POST_INSERT.toString() + " - " + types[0] + " - "
                  + listener.getClass().getName());
            }
          }

          if (aMethod.getAnnotation(PreUpdate.class) != null) {
            if (!preUpdate.containsKey(types[0])) {
              preUpdate.put(types[0], new HashMap<Method, EntityListener>());
            }
            preUpdate.get(types[0]).put(aMethod, listener);

            if (log.isDebugEnabled()) {
              log.debug("EventListener Registration : " + EventType.PRE_UPDATE.toString() + " - " + types[0] + " - "
                  + listener.getClass().getName());
            }
          }

          if (aMethod.getAnnotation(PostUpdate.class) != null) {
            if (!postUpdate.containsKey(types[0])) {
              postUpdate.put(types[0], new HashMap<Method, EntityListener>());
            }
            postUpdate.get(types[0]).put(aMethod, listener);

            if (log.isDebugEnabled()) {
              log.debug("EventListener Registration : " + EventType.POST_INSERT.toString() + " - " + types[0] + " - "
                  + listener.getClass().getName());
            }
          }

          if (aMethod.getAnnotation(PreRemove.class) != null) {
            if (!preRemove.containsKey(types[0])) {
              preRemove.put(types[0], new HashMap<Method, EntityListener>());
            }
            preRemove.get(types[0]).put(aMethod, listener);

            if (log.isDebugEnabled()) {
              log.debug("EventListener Registration : " + EventType.PRE_DELETE.toString() + " - " + types[0] + " - "
                  + listener.getClass().getName());
            }
          }

          if (aMethod.getAnnotation(PostRemove.class) != null) {
            if (!postRemove.containsKey(types[0])) {
              postRemove.put(types[0], new HashMap<Method, EntityListener>());
            }
            postRemove.get(types[0]).put(aMethod, listener);

            if (log.isDebugEnabled()) {
              log.debug("EventListener Registration : " + EventType.POST_DELETE.toString() + " - " + types[0] + " - "
                  + listener.getClass().getName());
            }
          }

          if (aMethod.getAnnotation(PostLoad.class) != null) {
            if (!postLoad.containsKey(types[0])) {
              postLoad.put(types[0], new HashMap<Method, EntityListener>());
            }
            postLoad.get(types[0]).put(aMethod, listener);

            if (log.isDebugEnabled()) {
              log.debug("EventListener Registration : " + EventType.POST_LOAD.toString() + " - " + types[0] + " - "
                  + listener.getClass().getName());
            }
          }
        }
      }
    }
  }

  /**
   * Execute the listeners. We need to check the entity's class, parent classes, and interfaces.
   * 
   * @param map
   * @param entity
   */
  private void execute(Map<Class<?>, Map<Method, EntityListener>> map, Object entity) {
    if (entity.getClass().isAnnotationPresent(Entity.class)) {

      // check for hits on this class or its superclasses.
      for (Class<?> entityClass = entity.getClass(); entityClass != null && entityClass != Object.class; entityClass =
          entityClass.getSuperclass()) {
        if (map.containsKey(entityClass)) {
          for (Map.Entry<Method, EntityListener> entry : map.get(entityClass).entrySet()) {
            try {
              entry.getKey().invoke(entry.getValue(), entity);
            } catch (InvocationTargetException | IllegalAccessException e) {
              log.error("Error occured while invoking event listener method(" + entry.getKey().getName()
                  + ") for persistance entity(" + entity + ")", e);
            }
          }
        }
      }

      // check for hits on interfaces.
      for (Class<?> c : entity.getClass().getInterfaces()) {
        if (map.containsKey(c)) {
          for (Map.Entry<Method, EntityListener> entry : map.get(c).entrySet()) {
            try {
              entry.getKey().invoke(entry.getValue(), entity);
            } catch (InvocationTargetException | IllegalAccessException e) {
              log.error("Error occured while invoking event listener method(" + entry.getKey().getName()
                  + ") for persistance entity(" + entity + ")", e);
            }
          }
        }
      }
    }
  }

  /**
   * @see org.hibernate.event.spi.PostDeleteEventListener#onPostDelete(org.hibernate
   *      .event.spi.PostDeleteEvent)
   */
  @Override
  public void onPostDelete(PostDeleteEvent event) {
    execute(postRemove, event.getEntity());
  }

  /**
   * @see org.hibernate.event.spi.PreDeleteEventListener#onPreDelete(org.hibernate
   *      .event.spi.PreDeleteEvent)
   */
  @Override
  public boolean onPreDelete(PreDeleteEvent event) {
    execute(preRemove, event.getEntity());
    return false;
  }

  /**
   * @see org.hibernate.event.spi.PreInsertEventListener#onPreInsert(org.hibernate
   *      .event.spi.PreInsertEvent)
   */
  @Override
  public boolean onPreInsert(PreInsertEvent event) {
    execute(preInsert, event.getEntity());
    return false;
  }

  /**
   * @see org.hibernate.event.spi.PostInsertEventListener#onPostInsert(org.hibernate
   *      .event.spi.PostInsertEvent)
   */
  @Override
  public void onPostInsert(PostInsertEvent event) {
    execute(postInsert, event.getEntity());
  }

  /**
   * @see org.hibernate.event.spi.PreUpdateEventListener#onPreUpdate(org.hibernate
   *      .event.spi.PreUpdateEvent)
   */
  @Override
  public boolean onPreUpdate(PreUpdateEvent event) {
    execute(preUpdate, event.getEntity());
    return false;
  }

  /**
   * @see org.hibernate.event.spi.PostUpdateEventListener#onPostUpdate(org.hibernate
   *      .event.spi.PostUpdateEvent)
   */
  @Override
  public void onPostUpdate(PostUpdateEvent event) {
    execute(postUpdate, event.getEntity());
  }

  /**
   * @see org.hibernate.event.spi.PostLoadEventListener#onPostLoad(org.hibernate
   *      .event.spi.PostLoadEvent)
   */
  @Override
  public void onPostLoad(PostLoadEvent event) {
    execute(postLoad, event.getEntity());
  }

  @Override
  public boolean requiresPostCommitHanding(EntityPersister persister) {
    return true;
  }
}
