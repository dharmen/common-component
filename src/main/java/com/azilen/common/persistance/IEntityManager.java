package com.azilen.common.persistance;

import java.util.Collection;
import java.util.List;

import com.azilen.common.persistance.id.ObjectId;

public interface IEntityManager<ET extends IEntity> {

  public void save(ET record);

  public void delete(ET record);

  public void deleteAll(Collection<ET> records);

  public ET getById(ObjectId id);

  public List<ET> getAll();

}
