package com.azilen.common.persistance;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.azilen.common.persistance.id.ObjectId;

public abstract class AbstractEntityManager<ET extends IEntity, DT extends IEntityDAO<ET>> implements
    IEntityManager<ET> {

  @Autowired
  protected DT dao;

  public DT getEntityDAO() {
    return dao;
  }

  public void setEntityDAO(DT dao) {
    this.dao = dao;
  }

  @Override
  @Transactional(readOnly = false)
  public void save(ET record) {

    getEntityDAO().save(record);

  }

  @Override
  @Transactional(readOnly = false)
  public void delete(ET record) {

    getEntityDAO().delete(record);

  }

  @Override
  @Transactional(readOnly = false)
  public void deleteAll(Collection<ET> records) {

    getEntityDAO().deleteAll(records);

  }

  @Override
  @Transactional(readOnly = true)
  public ET getById(ObjectId id) {

    ET returnIEntityType = getEntityDAO().getById(id);

    return returnIEntityType;
  }

  @Override
  @Transactional(readOnly = true)
  public List<ET> getAll() {

    return getEntityDAO().getAll();

  }

}
