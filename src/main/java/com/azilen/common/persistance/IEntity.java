package com.azilen.common.persistance;

import com.azilen.common.persistance.id.ObjectId;

public interface IEntity {

  public ObjectId getId();

}
