package com.azilen.common.persistance.id;

import java.io.Serializable;

public class ObjectId implements Serializable, Comparable<ObjectId> {

  private static final long serialVersionUID = 6211920058900708435L;
  private String id;

  public ObjectId() {

  }

  public ObjectId(String id) {
    this.id = id;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof ObjectId)) {
      return false;
    }

    ObjectId otherInstance = (ObjectId) object;

    if (otherInstance.getId() == null) {
      return false;
    }
    return otherInstance.getId().equals(this.getId());
  }

  @Override
  public final int hashCode() {
    if (getId() == null) {
      return 0;
    }
    return this.getId().hashCode();
  }

  @Override
  public String toString() {
    return id;
  }

  @Override
  public int compareTo(ObjectId object) {
    if (object != null) {
      return this.getId().compareTo(object.getId());
    }
    throw new IllegalArgumentException("Object in the parameter can not be null");
  }
}
