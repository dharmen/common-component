package com.azilen.common.persistance.id;

import java.io.Serializable;
import java.util.Properties;

import org.hibernate.dialect.Dialect;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.Configurable;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.id.UUIDHexGenerator;
import org.hibernate.type.Type;

public class ObjectIdGenerator implements IdentifierGenerator, Configurable {

  UUIDHexGenerator uuidGenerator;

  public ObjectIdGenerator() {
    uuidGenerator = new UUIDHexGenerator();
  }

  public void configure(Type type, Properties params, Dialect dialect) {
    uuidGenerator.configure(type, params, dialect);
  }

  public Serializable generate(SessionImplementor session, Object obj) {

    String s = uuidGenerator.generate(session, obj).toString();
    Serializable returnSerializable = new ObjectId(s);

    return returnSerializable;
  }

}
