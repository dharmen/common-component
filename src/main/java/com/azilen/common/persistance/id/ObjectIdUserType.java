package com.azilen.common.persistance.id;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.usertype.UserType;

public class ObjectIdUserType implements UserType, Serializable {

  private static final long serialVersionUID = 1L;

  private static final int[] TYPES = {Types.VARCHAR};
  private static final Class<? extends Serializable> RETURNED_CLASS = ObjectId.class;

  @Override
  public int[] sqlTypes() {
    return TYPES;
  }

  @Override
  public Class<?> returnedClass() {
    return RETURNED_CLASS;
  }

  @Override
  public boolean equals(Object arg0, Object arg1) throws HibernateException {
    if ((arg0 != null && arg1 != null)
        && ((arg0.getClass().equals(RETURNED_CLASS)) && (arg1.getClass().equals(RETURNED_CLASS)))) {
      ObjectId objectId0 = (ObjectId) arg0;
      ObjectId objectId1 = (ObjectId) arg1;

      if (objectId0.equals(objectId1))
        return true;
    }

    return false;
  }

  @Override
  public Object nullSafeGet(ResultSet rs, String[] names, SessionImplementor session, Object owner)
      throws HibernateException, SQLException {

    String colName = names[0];
    String id = rs.getString(colName);
    if (id == null) {
      return null;
    }
    ObjectId objectId = new ObjectId(id);

    return objectId;
  }

  @Override
  public void nullSafeSet(PreparedStatement pstmt, Object value, int index, SessionImplementor session)
      throws HibernateException, SQLException {

    if (value == null) {
      pstmt.setNull(index, TYPES[0]);
    } else if (value instanceof ObjectId) {
      ObjectId objectId = (ObjectId) value;
      pstmt.setString(index, objectId.getId());
    }

  }

  @Override
  public boolean isMutable() {
    return false;
  }

  @Override
  public Object deepCopy(Object o) throws HibernateException {
    if (o == null) {
      return null;
    }
    if (!(o instanceof ObjectId)) {
      throw new HibernateException("Can not deepcopy " + o.getClass() + " to ObjectId.class");
    }
    ObjectId subject = (ObjectId) o;
    ObjectId result = new ObjectId(subject.getId());

    return result;
  }

  @Override
  public Object assemble(Serializable arg0, Object arg1) throws HibernateException {
    if (arg0 == null) {
      return null;
    } else {
      return deepCopy(arg0);
    }
  }

  @Override
  public Serializable disassemble(Object arg0) throws HibernateException {
    if (arg0 == null) {
      return null;
    } else {
      return (Serializable) deepCopy(arg0);
    }
  }

  @Override
  public int hashCode(Object object) throws HibernateException {
    if (!(object instanceof ObjectId)) {
      throw new HibernateException("object must be instance of ObjectId.class");
    }
    ObjectId id = (ObjectId) object;

    return id.hashCode();
  }

  @Override
  public Object replace(Object original, Object target, Object owner) throws HibernateException {
    return deepCopy(original);
  }

}
